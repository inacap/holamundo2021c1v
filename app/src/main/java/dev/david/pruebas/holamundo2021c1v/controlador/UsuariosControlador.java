package dev.david.pruebas.holamundo2021c1v.controlador;

import android.content.ContentValues;
import android.content.Context;

import dev.david.pruebas.holamundo2021c1v.modelo.BDPrincipalContrato;
import dev.david.pruebas.holamundo2021c1v.modelo.UsuariosModelo;

public class UsuariosControlador {
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context ctx){
        this.capaModelo = new UsuariosModelo(ctx);
    }

    public String crearUsuario(String nombre, String password, String password2){

        // Validar los datos, validar contraseñas
        if(!password.equals(password2)){
            return "ERR_MATCH";
        }

        // Validar largo de la contraseña
        if(password.length() < 8){
            return "ERR_LARGO";
        }

        this.capaModelo.crearUsuario(nombre, password);

        return "OK";
    }

    public boolean procesarLogin(String nombre, String password){
        // Pedir los datos a la BD
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorNombre(nombre);

        // Si el usuario no existe
        if(usuario == null){
            return false;
        }

        //Validar las contraseñas
        String password_bd = usuario.getAsString(BDPrincipalContrato.TablaUsuarios.COL_PASSWORD);

        if(password.equals(password_bd)){
            return true;
        }

        return false;
    }
}
