package dev.david.pruebas.holamundo2021c1v.modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDPrincipalAsistente extends SQLiteOpenHelper {

    private final String crearTabla =
            "CREATE TABLE " + BDPrincipalContrato.TablaUsuarios.NOMBRE_TABLA + " (" +
                    BDPrincipalContrato.TablaUsuarios._ID + " INTEGER PRIMARY KEY, " +
                    BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO + " TEXT, " +
                    BDPrincipalContrato.TablaUsuarios.COL_PASSWORD + " TEXT)";

    public BDPrincipalAsistente(Context ctx){
        super(ctx, BDPrincipalContrato.NOMBRE_BD, null, BDPrincipalContrato.VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(this.crearTabla);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // TODO Averiguar como implementar el onUpdrade
    }
}
