package dev.david.pruebas.holamundo2021c1v.vista;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import dev.david.pruebas.holamundo2021c1v.R;
import dev.david.pruebas.holamundo2021c1v.vista.fragmentos.BienvenidaFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity implements BienvenidaFragment.OnFragmentInteractionListener {

    private int contador = 0;
    private BienvenidaFragment fragmento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Leer datos de la sesion
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
        String nombreUsuario = sesion.getString("nombreUsuario", null);

        // Llamar a los componentes visuales
        TextView tvMensaje = findViewById(R.id.mensajeBienvenida);
        Button btnLogout = findViewById(R.id.btnLogout);

        tvMensaje.setText("Bienvenido a casa, " + nombreUsuario + "!");

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambiar el estado de la preferencia compartida y asi cerrar sesion
                // Abrir archivo de SharedPreferences
                SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                // poner el archivo en modo escritura
                SharedPreferences.Editor editor = sesion.edit();

                // modificar el valor de la variable boolean
                editor.putBoolean("inicioSesion", false);
                editor.putString("nombreUsuario", null);

                // guardar
                editor.commit();

                Intent regreso = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(regreso);


                Toast.makeText(getApplicationContext(), "Hasta pronto!", Toast.LENGTH_LONG).show();

                // finalizar esta activity
                finish();
            }
        });

        // Mostrar el fragment de Binevenida
        // Instanciar el administrador
        FragmentManager adminFragmentos = getSupportFragmentManager();

        // Instanciar el fragmento
        fragmento = new BienvenidaFragment();

        // posicionar el fragmento
        adminFragmentos.beginTransaction().replace(R.id.flPrincipal, fragmento).commit();

        Button btnMensaje = findViewById(R.id.btnMensaje);
        btnMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contador++;
                fragmento.recibirMensaje("Ha hecho click " + contador + " veces");
            }
        });

    }

    @Override
    public void onFragmentInteraction() {
        contador = 1;
        fragmento.recibirMensaje("Ha hecho click " + contador + " veces");
    }
}