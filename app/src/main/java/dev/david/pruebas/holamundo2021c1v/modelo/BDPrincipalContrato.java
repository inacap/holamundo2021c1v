package dev.david.pruebas.holamundo2021c1v.modelo;

import android.provider.BaseColumns;

public class BDPrincipalContrato {
    private BDPrincipalContrato(){}

    public static final String NOMBRE_BD = "BDPrincipal.bd";
    public static final int VERSION_BD = 1;

    public static class TablaUsuarios implements BaseColumns {
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COL_NOMBRE_USUARIO = "nombreUsuario";
        public static final String COL_PASSWORD = "password";
    }
}
