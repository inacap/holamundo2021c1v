package dev.david.pruebas.holamundo2021c1v.vista;

import androidx.appcompat.app.AppCompatActivity;
import dev.david.pruebas.holamundo2021c1v.R;
import dev.david.pruebas.holamundo2021c1v.controlador.UsuariosControlador;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Verificar si habia iniciado sesion
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

        boolean inicioSesion = sesion.getBoolean("inicioSesion", false);

        if(inicioSesion){
            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }

        EditText etUsername = findViewById(R.id.etUsername);
        EditText etPassword = findViewById(R.id.etPassword);
        TextView tvRegistrar = findViewById(R.id.tvRegistrar);
        TextView tvBienvenida = findViewById(R.id.mensajeBienvenida);
        tvBienvenida.setText("Bienvenido!!");

        // Reaccionar a la opcion de crear cuenta
        tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Pasar a la ventana (Activity) de registro
                Intent segundaActivity = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(segundaActivity);
            }
        });

        // Llamar al boton de la UI
        Button btnIngresar = findViewById(R.id.btnIngresar);
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // 1.- Mostrar al usuario de forma permanente
                String nombreUsuario = etUsername.getText().toString();
                String password = etPassword.getText().toString();

                // Comunico con la capa controlador
                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());
                boolean inicioSesion = capaControlador.procesarLogin(nombreUsuario, password);

                if(inicioSesion){
                    // Registrar que alguien inicio sesion
                    // abrir el archivo
                    SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);

                    // activar el modo escritura
                    SharedPreferences.Editor editorSesion = sesion.edit();

                    // Almacenar el dato
                    editorSesion.putBoolean("inicioSesion", true);
                    editorSesion.putString("nombreUsuario", nombreUsuario);

                    // guardar y cerrar
                    editorSesion.commit();

                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}