package dev.david.pruebas.holamundo2021c1v.vista.fragmentos;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import dev.david.pruebas.holamundo2021c1v.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BienvenidaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BienvenidaFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TextView tvBienvenida;
    private OnFragmentInteractionListener interactionListener;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BienvenidaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BienvenidaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BienvenidaFragment newInstance(String param1, String param2) {
        BienvenidaFragment fragment = new BienvenidaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View interfaz = inflater.inflate(R.layout.fragment_bienvenida, container, false);

        // Instanciar los componentes visuales
        tvBienvenida = interfaz.findViewById(R.id.tvBienvenida);

        tvBienvenida.setText("Texto desde la clase");

        tvBienvenida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactionListener.onFragmentInteraction();
            }
        });

        Button btnDescarga = interfaz.findViewById(R.id.btnPedirDatos);
        btnDescarga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Solicitar datos de los indicadores economicos
                // Protocolo: HTPPS
                // Host: mindicador.cl
                // Recurso: /api
                // Metodo: GET

                String url = "https://mindicador.cl/api";

                // Construir la solicitud
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Procesar los datos
                                Log.i("RESPUESTA", response);

                                try {
                                    JSONObject respuesta = new JSONObject(response);
                                    String autor = respuesta.getString("autor");
                                    JSONObject dolar = respuesta.getJSONObject("dolar");
                                    double valorDolar = dolar.getDouble("valor");
                                    String umDolar = dolar.getString("unidad_medida");
                                    Toast.makeText(getContext(), "El dolar vale: " + valorDolar + " " + umDolar, Toast.LENGTH_SHORT).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: programar el error
                            }
                        }
                );

                // Enviar la solicitud
                RequestQueue listaEspera = Volley.newRequestQueue(getContext());
                listaEspera.add(solicitud);
            }
        });

        return interfaz;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        interactionListener = (OnFragmentInteractionListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    public void recibirMensaje(String texto){
        tvBienvenida.setText(texto);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }
}