package dev.david.pruebas.holamundo2021c1v.vista;

import androidx.appcompat.app.AppCompatActivity;
import dev.david.pruebas.holamundo2021c1v.R;
import dev.david.pruebas.holamundo2021c1v.controlador.UsuariosControlador;
import dev.david.pruebas.holamundo2021c1v.modelo.UsuariosModelo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        EditText etNombreUsuario = findViewById(R.id.etNombreUsuario);
        EditText etPassword = findViewById(R.id.etPassword1);
        EditText etPassword2 = findViewById(R.id.etPassword2);
        Button btRegistrarme = findViewById(R.id.btRegistrarme);

        btRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreUsuario = etNombreUsuario.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                String password2 = etPassword2.getText().toString().trim();

                // establecer comunicacion con la capa controlador
                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());
                String registrar = capaControlador.crearUsuario(nombreUsuario, password, password2);

                switch(registrar){
                    case "OK":
                        Toast.makeText(getApplicationContext(), "Usuario creado", Toast.LENGTH_LONG).show();
                        finish();
                        break;

                    case "ERR_MATCH":
                        etPassword2.setError("Las contraseñas deben ser iguales");
                        break;

                    case "ERR_LARGO":
                        etPassword.setError("La contraseña debe tener al menos 8 caracteres");
                        break;
                }

            }
        });
    }
}