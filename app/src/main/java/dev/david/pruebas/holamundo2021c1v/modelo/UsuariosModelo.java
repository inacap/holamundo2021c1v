package dev.david.pruebas.holamundo2021c1v.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class UsuariosModelo {
    /**
     * Gestionar las transacciones de la tabla usuarios
     */

    private BDPrincipalAsistente bdAsistente;

    public UsuariosModelo(Context context){
        this.bdAsistente = new BDPrincipalAsistente(context);
    }

    /**
     * Insertar datos en la tabla
     */
    public boolean crearUsuario(String nombre, String password){

        // objeto a almacenar en la tabla
        ContentValues usuario = new ContentValues();
        usuario.put(BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO, nombre);
        usuario.put(BDPrincipalContrato.TablaUsuarios.COL_PASSWORD, password);

        Log.i("USUARIO NUEVO", usuario.toString());

        // Abrir comunicacion
        SQLiteDatabase bdCon = this.bdAsistente.getWritableDatabase();

        // Almacenar los datos
        bdCon.insert(BDPrincipalContrato.TablaUsuarios.NOMBRE_TABLA, null, usuario);

        // Cerrar el archivo
        bdCon.close();

        return true;
    }

    public ContentValues obtenerUsuarioPorNombre(String nombre){
        // Abrir comunicacion
        SQLiteDatabase bdCon = this.bdAsistente.getReadableDatabase();

        // SELECT * FROM usuarios WHERE nombreUsuario = 'pepe' LIMIT 1

        // Projection
        String[] projection = {
                BDPrincipalContrato.TablaUsuarios._ID,
                BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO,
                BDPrincipalContrato.TablaUsuarios.COL_PASSWORD
        };

        // Condiciones
        String condiciones = BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO + " = ? LIMIT 1";

        // Valores para el filtro
        String[] valoresCondiciones = { nombre };

        Cursor resultado = bdCon.query(
                BDPrincipalContrato.TablaUsuarios.NOMBRE_TABLA,
                projection,
                condiciones,
                valoresCondiciones,
                null, null, null
        );

        if(resultado.moveToFirst()){
            // Existen datos
            //String nombre_bd = resultado.getString(1);
            String nombre_bd = resultado.getString(resultado.getColumnIndex(BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO));
            String password_bd = resultado.getString(resultado.getColumnIndex(BDPrincipalContrato.TablaUsuarios.COL_PASSWORD));

            // crear el objeto a retornar
            ContentValues usuario_bd = new ContentValues();
            usuario_bd.put(BDPrincipalContrato.TablaUsuarios.COL_NOMBRE_USUARIO, nombre_bd);
            usuario_bd.put(BDPrincipalContrato.TablaUsuarios.COL_PASSWORD, password_bd);

            return usuario_bd;
        }

        // No hay datos

        return null;
    }
}
